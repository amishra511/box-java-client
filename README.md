# README #

Java SDK for accessing Box APIs

### What is this repository for? ###

* A java library which provides easy access to Box storage data using Box content api. It also includes a full OAuth2 implementation which allows to get access  to users' accounts without asking them for their account credentials
* Version 1.0

### How do I get set up? ###

* Configuration: The code was developed in eclipse and hence can be imported directly into eclipse workspace as a project. No special configuration is required. 
* Dependencies: All the dependent libraries are included in the lib folder
* Database configuration: No database configuration required to run the code.
* How to run tests: There are some sample test files included in the test folder
* Deployment instructions: All the outputs are console based hence no deployment on any server is required. For reference some test files are included in the code. 

