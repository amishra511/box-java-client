package com.boxclient.common;

public class BoxEnums {
	public enum GrantType {
		AUTH_CODE("authorization_code"),
		REF_TOKEN("refresh_token"),
		PROVISION("urn:box:oauth2:grant-type:provision");
		GrantType(String value){			
		}
	}
	public enum AccessLevel{
		open,
		company,
		collaborators		
	}
	
	public enum SyncState{
		synced,
		not_synced,
		partially_synced
	}
	public enum CanPreview{
		open,
		company
	}
	
	public enum CollabType{
		user,
		group
	}
	
	public enum CommentType{
		file,
		comment
	}
	
	public enum CollabStatus{
		accepted,
		pending,
		rejected
	}
	
	public enum CollabRole{
		EDITOR("editor"),
		VIEWER("viewer"),
		PREVIEWER("previewer"),
		UPLOADER("uploader"),
		PREV_UPLOADER("previewer uploader"),
		VIEW_UPLOADER("viewer uploader"),
		COOWNER("co-owner"),
		OWNER("owner");
		CollabRole (String value){			
		}
	}
	
	public enum ThumbnailSize{
		THIRTY_TWO(32),
		SIXTY_FOUR(64),
		ONE_TWENTY_EIGHT(128),
		TWO_FIFTY_SIX(256);		
		ThumbnailSize(int val){
			
		}
	}
	
//	public enum Permissions{
//		can_download,
//		can_upload,
//		can_rename,
//		can_delete,
//		can_share,
//		can_invite_collaborator,
//		can_set_share_access
//		
//	}
	public enum User_Events{
		ITEM_CREATE,
		ITEM_UPLOAD,
		COMMENT_CREATE,
		ITEM_DOWNLOAD,
		ITEM_PREVIEW,
		ITEM_MOVE,
		ITEM_COPY,
		TASK_ASSIGNMENT_CREATE,
		LOCK_CREATE,
		LOCK_DESTROY,
		ITEM_TRASH,
		ITEM_UNDELETE_VIA_TRASH,
		COLLAB_ADD_COLLABORATOR,
		COLLAB_ROLE_CHANGE,
		COLLAB_INVITE_COLLABORATOR,
		COLLAB_REMOVE_COLLABORATOR,
		ITEM_SYNC,
		ITEM_UNSYNC,
		ITEM_RENAME,
		ITEM_SHARED_CREATE,
		ITEM_SHARED_UNSHARE,
		ITEM_SHARED,
		TAG_ITEM_CREATE,
		ADD_LOGIN_ACTIVITY_DEVICE,
		REMOVE_LOGIN_ACTIVITY_DEVICE,
		CHANGE_ADMIN_ROLE
	}
	
	public enum Enterprise_Events{
		GROUP_ADD_USER,
		NEW_USER,
		GROUP_CREATION,
		GROUP_DELETION,
		DELETE_USER,
		GROUP_EDITED,
		EDIT_USER,
		GROUP_ADD_FOLDER,
		GROUP_REMOVE_USER,		
		GROUP_REMOVE_FOLDER,
		ADMIN_LOGIN,
		ADD_DEVICE_ASSOCIATION,
		FAILED_LOGIN,
		LOGIN,
		USER_AUTHENTICATE_OAUTH2_TOKEN_REFRESH,
		REMOVE_DEVICE_ASSOCIATION,
		TERMS_OF_SERVICE_AGREE,
		TERMS_OF_SERVICE_REJECT,
		COPY,
		DELETE,
		DOWNLOAD,
		EDIT,
		LOCK,
		MOVE,
		PREVIEW,
		RENAME,
		STORAGE_EXPIRATION,
		UNDELETE,
		UNLOCK,
		UPLOAD,
		SHARE,
		ITEM_SHARED_UPDATE,
		UPDATE_SHARE_EXPIRATION,
		SHARE_EXPIRATION,
		UNSHARE,
		COLLABORATION_ACCEPT,
		COLLABORATION_ROLE_CHANGE,
		UPDATE_COLLABORATION_EXPIRATION,
		COLLABORATION_REMOVE,
		COLLABORATION_INVITE,
		COLLABORATION_EXPIRATION,
		ITEM_SYNC,
		ITEM_UNSYNC
	}
	
	public enum ContentType{
		name,
		description,
		file_content,
		comments,
		tags
	}
	
	public enum User_Role{
		admin,
		coadmin,
		user
	}
	
	public enum User_Status{
		active,
		inactive,
		cannot_delete_edit,
		cannot_delete_edit_upload
	}
	
	public enum Task_Resolution_state{
		completed,
		incomplete,
		approved,
		rejected
	}
	public BoxEnums() {
		// TODO Auto-generated constructor stub
	}

}
