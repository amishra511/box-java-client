package com.boxclient.common;

import java.sql.Timestamp;
import java.util.List;

import com.boxclient.types.Collaboration;
import com.boxclient.types.Comment;
import com.boxclient.types.ContentMetadata;
import com.boxclient.types.EmailAlias;
import com.boxclient.types.Enterprise;
import com.boxclient.types.FileVersion;
import com.boxclient.types.Invite;
import com.boxclient.types.Lock;
import com.boxclient.types.MiniContentMetadata;
import com.boxclient.types.MiniUser;
import com.boxclient.types.SharedLink;
import com.boxclient.types.Task;
import com.boxclient.types.User;
import com.boxclient.types.collection.BaseCollection;
import com.boxclient.types.collection.CollaborationCollection;
import com.boxclient.types.collection.CommentCollection;
import com.boxclient.types.collection.ContentMetadataCollection;
import com.boxclient.types.collection.EmailAliasCollection;
import com.boxclient.types.collection.FileVersionCollection;
import com.boxclient.types.collection.MiniContentMetadataCollection;
import com.boxclient.types.collection.TaskCollection;
import com.boxclient.types.param.AdvanceSearchOptParams;

public interface BoxClient {

	public MiniContentMetadataCollection getFolderItems(String folderId);

	// By default, only specific information about folder contents
	// (MiniContent's) is returned data.
	// Field param is used to specify additional attributes which are not
	// returned by default
	public MiniContentMetadataCollection getFolderItems(String folderId,
			List<String> fields);

	// Paginated results can be retrieved using limit and offset params
	public MiniContentMetadataCollection getFolderItems(String folderId,
			List<String> fields, Integer limit, Integer offset);

	public ContentMetadata getFolderMetadata(String folderId);

	public ContentMetadata getFolderMetadata(String folderId,
			List<String> fields);

	public ContentMetadata createFolder(String name, String parentId);

	public ContentMetadata updateFolderMetadata(String folderId,
			ContentMetadata metadata);

	public void deleteFolder(String folderId);// Depending on the enterprise
												// settings for this user, the
												// folder will either be
												// actually deleted from Box or
												// moved to the trash.

	public ContentMetadata copyFolder(String srcFolderId, String destFolderId);
	
	public ContentMetadata moveFolder(String folderId, String newParentId);

	public ContentMetadata createFolderSharedLink(String folderId, SharedLink sl);

	public CollaborationCollection getFolderCollaborations(String folderId);

	// Trash
	public ContentMetadataCollection getAllTrashedItems();

	public ContentMetadata getTrashItem(String path);

	public void deleteTrashItem(String path);

	// newName is optional, could be the same name as current.
	public ContentMetadata restoreTrashItem(String path, String newName);

	// File
	public ContentMetadata getFileContent(String fileId);

	public ContentMetadata getFileMetadata(String fileId);

	public ContentMetadata getFileMetadata(String fileId, List<String> fields);

	public ContentMetadata updateFileMetadata(String fileId,
			ContentMetadata metadata);// use of if_match

	public void lockFile(String fileId, Lock lck);

	public void unlockFile(String fileId);

	public void downloadFile(String fileId, String saveToFile);

	public void downloadFile(String fileId, String saveToFile, String versionNo);

	// set fileSize=0 for unknown sizes
	public Integer preUploadCheck(String fileName, String parentId,
			String fileSize);

	public ContentMetadata uploadFile(String filePath, String fileName,
			String parentId);

	public ContentMetadata uploadFile(String filePath, String fileName,
			String parentId, Timestamp createdAt, Timestamp modifiedAt);

	public ContentMetadata uploadFileWithoutVersionConflict(String filePath,
			String fileName, String parentId, String etag);

	public ContentMetadata uploadFileWithoutVersionConflict(String filePath,
			String fileName, String parentId, Timestamp createdAt,
			Timestamp modifiedAt, String etag);

	// Only available to premium account users
	public FileVersionCollection getFileVersions(String fileId);

	public FileVersion promoteFileVersion(String fileId, String versionId);

	// Depending on the enterprise settings for this user, the content will
	// either be actually deleted from Box or moved to the trash.
	public void deleteFile(String fileId);

	public void deleteFile(String fileId, String versionId);

	public ContentMetadata copyFile(String srcFileId, String destFolderId);
	

	public void getFileThumbnail(String fileId);

	public ContentMetadata createFileSharedLink(String fileId, SharedLink sl);

	public CommentCollection getFileComments(String fileId);

	public TaskCollection getFileTasks(String fileId);

	public ContentMetadata getContentAsUser(String path, String userId);

	public ContentMetadata getContentAsUser(String path, String fields,
			String userId);

	public ContentMetadata getContentAsUser(String path, Integer limit,
			Integer offset, String userId);

	public ContentMetadata getContentAsUser(String path, String fields,
			Integer limit, Integer offset, String userId);

	// User
	public User getEnterpriseUser(String userId);

	public User getCurrentUser();

	public List<User> getAllEnterpriseUsers();

	public User createEnterpriseUser(User user);

	public User updateEnterpriseUser(User user);

	public void deleteEnterpriseUser(User user);

	// Only folder_id =0 is supported for moving folder between users
	public ContentMetadata moveFolderBtwnUsers(MiniUser srcUser,
			MiniUser destUser, String folderId);

	public User changeUserPrimaryLogin(String userId, String email);

	public EmailAliasCollection getUserEmailAliases(String userId);

	public EmailAlias addUserEmailAlias(String userId, String email);

	public void removeUserEmailAlias(String userId, String emailAliasId);

	public Invite inviteUserToJoin(Enterprise ent, MiniUser user);

	public Comment getComment();

	public Collaboration getCollaboration();

	// Search
	public ContentMetadataCollection simpleSearch(String query);

	public ContentMetadataCollection advanceSearch(String query,
			AdvanceSearchOptParams searchOptParam);
}
