package com.boxclient.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.security.*;
import java.sql.Timestamp;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import com.boxclient.exception.BaseException;
import com.boxclient.exception.BoxException;
import com.boxclient.exception.OAuthException;
import com.boxclient.json.JSONObject;
import com.boxclient.oauth.OAuthClientImpl;
import com.boxclient.types.BoxError;
import com.boxclient.types.Collaboration;
import com.boxclient.types.Comment;
import com.boxclient.types.ContentMetadata;
import com.boxclient.types.EmailAlias;
import com.boxclient.types.Enterprise;
import com.boxclient.types.FileVersion;
import com.boxclient.types.Invite;
import com.boxclient.types.Lock;
import com.boxclient.types.MiniContentMetadata;
import com.boxclient.types.MiniUser;
import com.boxclient.types.SharedLink;
import com.boxclient.types.Task;
import com.boxclient.types.User;
import com.boxclient.types.collection.BaseCollection;
import com.boxclient.types.collection.CollaborationCollection;
import com.boxclient.types.collection.CommentCollection;
import com.boxclient.types.collection.ContentMetadataCollection;
import com.boxclient.types.collection.EmailAliasCollection;
import com.boxclient.types.collection.FileVersionCollection;
import com.boxclient.types.collection.MiniContentMetadataCollection;
import com.boxclient.types.collection.TaskCollection;
import com.boxclient.types.param.AdvanceSearchOptParams;
import com.boxclient.util.BoxUtils;
import com.boxclient.util.HttpUtils;
import com.boxclient.util.StringUtils;

public class BoxMain implements BoxClient {
	private static final String AUTHORIZE_URL = "https://app.box.com/api/oauth2/authorize?";
	private static final String TOKEN_URL = "https://app.box.com/api/oauth2/token";
	private static final String CONTENT_URL = "https://api.box.com/2.0";
	private static final String UPLOAD_URL = "https://upload.box.com/api/2.0/files/content";

	// https://api.box.com/2.0/users/me
	private String accessToken;
	private DefaultJsonMapper jsonMapper;

	public BoxMain(String accessToken) {
		this.accessToken = accessToken;
		this.jsonMapper = new DefaultJsonMapper();
	}

	@Override
	public MiniContentMetadataCollection getFolderItems(String folderId) {
		return getFolderItems(folderId, null, null, null);
	}

	@Override
	public MiniContentMetadataCollection getFolderItems(String folderId,
			List<String> fields) {
		return getFolderItems(folderId, fields, null, null);
	}

	@Override
	public MiniContentMetadataCollection getFolderItems(String folderId,
			List<String> fields, Integer limit, Integer offset) {
		try {
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("Authorization", "Bearer " + accessToken);
			StringBuilder url = new StringBuilder(CONTENT_URL);
			if (null != folderId) {
				url.append("/folders/" + folderId + "/items");
			} else {
				throw new BoxException("Folder id can not be null");
			}
			if (null != fields) {
				if (fields.size() > 0) {
					StringBuilder fieldsParam = new StringBuilder("?fields=");
					for (String field : fields) {
						fieldsParam.append(field + ",");
					}
					// Substring to get rid of last ','
					url.append(fieldsParam.substring(0,
							fieldsParam.length() - 1));
				}
			}
			if (null != limit) {
				url.append("&limit=" + limit);
			}
			if (null != offset) {
				url.append("&offset=" + offset);
			}
			HttpResponse response = HttpUtils.executeGet(url.toString(),
					headers);
			HttpEntity entity = response.getEntity();
			String output = EntityUtils.toString(entity);
			if (StringUtils.isBlank(output)) {
				throw new BoxException(
						"Empty response from Box API. Verify the parameter values");
			}
			if (!BoxUtils.returnedError(output)) {
				return (MiniContentMetadataCollection) jsonMapper.toJavaObject(
						output, MiniContentMetadataCollection.class);
			} else {
				throw new BoxException(getErrorDetails(output));
			}

		} catch (Exception e) {
			throw new BoxException("Excption while fetching folder contents: ",
					e);
		}
	}

	@Override
	public ContentMetadata getFolderMetadata(String folderId) {
		return getMetadata("/folders/" + folderId, null);
	}

	@Override
	public ContentMetadata getFolderMetadata(String folderId,
			List<String> fields) {
		return getMetadata("/folders/" + folderId, fields);
	}

	protected ContentMetadata getMetadata(String path, List<String> fields) {
		String output = null;
		try {
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("Authorization", "Bearer " + accessToken);
			StringBuilder url = new StringBuilder(CONTENT_URL);
			if (null != path) {
				url.append(path);
			} else {
				throw new BaseException("Content path can not be null");
			}
			if (null != fields) {
				if (fields.size() > 0) {
					StringBuilder fieldsParam = new StringBuilder("?fields=");
					for (String field : fields) {
						fieldsParam.append(field + ",");
					}
					// Substring to get rid of last ','
					url.append(fieldsParam.substring(0,
							fieldsParam.length() - 1));
				}
			}
			HttpResponse response = HttpUtils.executeGet(url.toString(),
					headers);
			HttpEntity entity = response.getEntity();
			output = EntityUtils.toString(entity);
			if (StringUtils.isBlank(output)) {
				throw new BoxException(
						"Empty response from Box API. Verify the parameter values");
			}
			if (!BoxUtils.returnedError(output)) {
				ContentMetadata foldMdata = jsonMapper.toJavaObject(output,
						ContentMetadata.class);
				return foldMdata;
			} else {
				throw new BoxException(getErrorDetails(output));
			}

		} catch (Exception exp) {

			throw new BoxException(
					"Exception while fetching folder metadata:  ", exp);
		}
	}

	@Override
	public ContentMetadata updateFolderMetadata(String folderId,
			ContentMetadata metadata) {
		// Root Folder (id =0) can not be updated
		try {
			StringBuilder url = new StringBuilder(CONTENT_URL);
			if (null != folderId) {
				url.append("/folders/" + folderId);
			} else {
				throw new BoxException("Folder id can not be null");
			}
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("Authorization", "Bearer " + accessToken);
			String body = jsonMapper.toJson(metadata, true);
			HttpResponse response = HttpUtils.executePut(url.toString(),
					headers, body);
			HttpEntity entity = response.getEntity();
			String output = EntityUtils.toString(entity);
			if (StringUtils.isBlank(output)) {
				throw new BoxException(
						"Empty response from Box API. Verify the put request");
			}
			if (!BoxUtils.returnedError(output)) {
				ContentMetadata foldMdata = jsonMapper.toJavaObject(output,
						ContentMetadata.class);
				return foldMdata;
			} else {
				throw new BoxException(getErrorDetails(output));
			}
		} catch (Exception e) {
			throw new BoxException(
					"Exception while updating folder metadata:  ", e);
		}
	}

	@Override
	public void deleteFolder(String folderId) {
		try {
			StringBuilder url = new StringBuilder(CONTENT_URL);
			if (null != folderId) {
				url.append("/folders/" + folderId + "?recursive=true");
			} else {
				throw new BoxException("Folder id can not be null");
			}
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("Authorization", "Bearer " + accessToken);
			HttpResponse response = HttpUtils.executeDelete(url.toString(),
					headers);
			HttpEntity entity = response.getEntity();
			if (null != entity) {
				String output = EntityUtils.toString(entity);
				if (BoxUtils.returnedError(output)) {
					throw new BoxException(getErrorDetails(output));
				}
			}
		} catch (Exception e) {
			throw new BoxException("Exception while deleting folder with id: "
					+ folderId, e);
		}
		// TODO Auto-generated method stub

	}

	@Override
	public ContentMetadata moveFolder(String folderId, String newParentId) {
		try{
			ContentMetadata fldMdata = new ContentMetadata();
			MiniContentMetadata parent = new MiniContentMetadata();
			if (null != newParentId) {
				parent.setId(newParentId);
			} else {
				throw new BoxException("Parent id can not be null");
			}
			fldMdata.setParent(parent);
			return updateFolderMetadata(folderId, fldMdata);
		} catch (Exception e) {
			throw new BoxException(
					"Exception while moving folder:  ", e);
		}
	}

	@Override
	public ContentMetadata copyFolder(String srcFolderId, String destFolderId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ContentMetadata createFolderSharedLink(String folderId, SharedLink sl) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ContentMetadata getContentAsUser(String path, String userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ContentMetadata getContentAsUser(String path, String fields,
			String userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ContentMetadata getContentAsUser(String path, Integer limit,
			Integer offset, String userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ContentMetadata getContentAsUser(String path, String fields,
			Integer limit, Integer offset, String userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ContentMetadata createFolder(String name, String parentId) {
		try {
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("Authorization", "Bearer " + accessToken);
			MiniContentMetadata body = new MiniContentMetadata();
			body.setName(name);
			MiniContentMetadata parent = new MiniContentMetadata();
			parent.setId(parentId);
			body.setParent(parent);
			String jsonBody = jsonMapper.toJson(body, true);
			HttpResponse response = HttpUtils.executePost(CONTENT_URL
					+ "/folders", null, headers, jsonBody);
			// ContentMetaData newFolder =
			HttpEntity entity = response.getEntity();
			String output = EntityUtils.toString(entity);
			if (StringUtils.isBlank(output)) {
				throw new BoxException(
						"Empty response from Box API. Verify the request body");
			}
			if (!BoxUtils.returnedError(output)) {
				return jsonMapper.toJavaObject(output, ContentMetadata.class);
			} else {
				throw new BoxException(getErrorDetails(output));
			}
		} catch (Exception e) {
			throw new BoxException("Exception while creating folder: ", e);
		}
	}

	@Override
	public CollaborationCollection getFolderCollaborations(String folderId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ContentMetadataCollection getAllTrashedItems() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ContentMetadata getTrashItem(String path) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteTrashItem(String path) {
		// TODO Auto-generated method stub

	}

	@Override
	public ContentMetadata restoreTrashItem(String path, String newName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ContentMetadata getFileContent(String fileId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ContentMetadata getFileMetadata(String fileId) {
		return getMetadata("/files/" + fileId, null);
	}

	@Override
	public ContentMetadata getFileMetadata(String fileId, List<String> fields) {
		return getMetadata("/files/" + fileId, fields);
	}

	@Override
	public ContentMetadata updateFileMetadata(String fileId,
			ContentMetadata metadata) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FileVersion promoteFileVersion(String fileId, String versionId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteFile(String fileId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteFile(String fileId, String versionId) {
		// TODO Auto-generated method stub

	}

	@Override
	public ContentMetadata copyFile(String srcFileId, String destFolderId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ContentMetadata createFileSharedLink(String fileId, SharedLink sl) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void lockFile(String fileId, Lock lck) {
		// TODO Auto-generated method stub

	}

	@Override
	public void unlockFile(String fileId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void downloadFile(String fileId, String saveToFile) {
		// TODO Auto-generated method stub
		try {
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("Authorization", "Bearer " + accessToken);
			HttpResponse response = HttpUtils.executeGet(CONTENT_URL
					+ "/files/" + fileId + "/content", headers);
			HttpEntity entity = response.getEntity();
			byte[] output = null;
			output = EntityUtils.toByteArray(entity);
			FileUtils.writeByteArrayToFile(new File(saveToFile), output);

		} catch (Exception e) {
			throw new BoxException("Excption while downloading file: ", e);
		}
		// response.

	}

	@Override
	public void downloadFile(String fileId, String saveToFile, String versionNo) {
		// TODO Auto-generated method stub

	}

	@Override
	public Integer preUploadCheck(String fileName, String parentId,
			String fileSize) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ContentMetadata uploadFile(String filePath, String fileName,
			String parentId) {
		return uploadFile(filePath, fileName, parentId, null, null);
	}

	@Override
	public ContentMetadata uploadFile(String filePath, String fileName,
			String parentId, Timestamp createdAt, Timestamp modifiedAt) {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(new File(filePath));
			String md5 = DigestUtils.md5Hex(fis);
			fis.close();
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("Authorization", "Bearer " + accessToken);
			// headers.put("Content-MD5", md5);
			MiniContentMetadata cont = new MiniContentMetadata();
			MiniContentMetadata parent = new MiniContentMetadata();
			parent.setId(parentId);
			cont.setName(fileName);
			if (null != createdAt) {
				cont.setCreatedAt(createdAt);
			}
			if (null != modifiedAt) {
				cont.setModifiedAt(modifiedAt);
			}
			cont.setParent(parent);
			StringBody attrParam = new StringBody(jsonMapper.toJson(cont, true));
			FileBody fileparam = new FileBody(new File(filePath));
			Map<String, ContentBody> parts = new HashMap<String, ContentBody>();
			parts.put("attributes", attrParam);
			parts.put("file", fileparam);
			HttpResponse response = HttpUtils.executeMultipartPost(UPLOAD_URL,
					parts, headers);
			HttpEntity entity = response.getEntity();
			String output = EntityUtils.toString(entity);
			if (StringUtils.isBlank(output)) {
				throw new BoxException(
						"Empty response from Box API. Verify the parameter values");
			}
			if (!BoxUtils.returnedError(output)) {
				return jsonMapper.toJavaObject(output, ContentMetadata.class);
			} else {
				throw new BoxException(getErrorDetails(output));
			}

		} catch (Exception exp) {
			throw new BoxException("Excption while uploading file", exp);
		} finally {
			if (null != fis) {
				try {
					fis.close();
				} catch (IOException e) {
					throw new BoxException(
							"Excption in closing FileInputStream: ", e);
				}
			}
		}

	}

	@Override
	public ContentMetadata uploadFileWithoutVersionConflict(String filePath,
			String fileName, String parentId, Timestamp createdAt,
			Timestamp modifiedAt, String etag) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ContentMetadata uploadFileWithoutVersionConflict(String filePath,
			String fileName, String parentId, String etag) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FileVersionCollection getFileVersions(String fileId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void getFileThumbnail(String fileId) {
		// TODO Auto-generated method stub

	}

	@Override
	public CommentCollection getFileComments(String fileId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TaskCollection getFileTasks(String fileId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getEnterpriseUser(String userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getCurrentUser() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> getAllEnterpriseUsers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User createEnterpriseUser(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User updateEnterpriseUser(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteEnterpriseUser(User user) {
		// TODO Auto-generated method stub

	}

	@Override
	public ContentMetadata moveFolderBtwnUsers(MiniUser srcUser,
			MiniUser destUser, String folderId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User changeUserPrimaryLogin(String userId, String email) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EmailAliasCollection getUserEmailAliases(String userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EmailAlias addUserEmailAlias(String userId, String email) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeUserEmailAlias(String userId, String emailAliasId) {
		try {

		} catch (Exception e) {
			throw new BoxException("Excption removing user email alias: ", e);
		}
	}

	@Override
	public Invite inviteUserToJoin(Enterprise ent, MiniUser user) {
		try {

		} catch (Exception e) {
			throw new BoxException(
					"Excption while sending invite to user to join: ", e);
		}
		return null;
	}

	@Override
	public Comment getComment() {
		try {

		} catch (Exception e) {
			throw new BoxException("Excption while fetching comment data: ", e);
		}
		return null;
	}

	@Override
	public Collaboration getCollaboration() {
		try {

		} catch (Exception e) {
			throw new BoxException(
					"Excption while fetching collaboration data: ", e);
		}
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ContentMetadataCollection simpleSearch(String query) {
		try {
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("Authorization", "Bearer " + accessToken);
			HttpResponse response = HttpUtils.executeGet(CONTENT_URL
					+ "/search?query=" + query, headers);
			HttpEntity entity = response.getEntity();
			String output = EntityUtils.toString(entity);
			if (StringUtils.isBlank(output)) {
				throw new BoxException(
						"Empty response from Box API. Verify the parameter values");
			}
			if (!BoxUtils.returnedError(output)) {
				return jsonMapper.toJavaObject(output,
						ContentMetadataCollection.class);
			} else {
				throw new BoxException(getErrorDetails(output));
			}
		} catch (Exception exp) {
			throw new BoxException("Excption while performing simple search: ",
					exp);
		}
	}

	@Override
	public ContentMetadataCollection advanceSearch(String query,
			AdvanceSearchOptParams searchOptParam) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getErrorDetails(String response) {
		try {
			BoxError error = jsonMapper.toJavaObject(response, BoxError.class);
			String errorMsg = null;
			if (null != error.getContextInfo().getErrors())
				errorMsg = error.getStatus().toString()
						+ " - "
						+ error.getContextInfo().getErrors().get(0)
								.getMessage();
			else
				errorMsg = error.getStatus().toString() + " - "
						+ error.getMessage();
			return errorMsg;
		} catch (Exception e) {
			return e.getMessage();
		}

	}

}
