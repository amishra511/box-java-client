package com.boxclient.util;

import com.boxclient.json.JSONObject;

public  class BoxUtils {
	
	public static boolean returnedError(String output) {
		if (!StringUtils.isBlank(output) && output.trim().startsWith("{")) {
			JSONObject jsonObject = new JSONObject(output);
			try {
				if (jsonObject.getString("type").equals("error")) {
					return true;
				}
			} catch (Exception exp) {
				return false;
			}
		}
		return false;
	}

}
