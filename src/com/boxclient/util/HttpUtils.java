package com.boxclient.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.boxclient.exception.BaseException;
import com.boxclient.exception.BoxException;

public class HttpUtils {

	public static HttpResponse executePost(String url,
			Map<String, String> params, Map<String, String> headers, String body) {
		HttpResponse response = null;
		try {
			HttpPost postRequest = new HttpPost(url);
			HttpClient client = new DefaultHttpClient();
			if (null != params) {
				if (params.size() > 0) {
					List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
					Iterator<Entry<String, String>> it = params.entrySet()
							.iterator();
					while (it.hasNext()) {
						Map.Entry pair = (Map.Entry) it.next();
						urlParameters.add(new BasicNameValuePair(pair.getKey()
								.toString(), pair.getValue().toString()));
					}
					postRequest.setEntity(new UrlEncodedFormEntity(
							urlParameters));
					// postRequest.set
				}
			}
			if (null != headers) {
				if (headers.size() > 0) {
					Iterator<Entry<String, String>> itHead = headers.entrySet()
							.iterator();
					while (itHead.hasNext()) {
						Map.Entry headPair = (Map.Entry) itHead.next();
						postRequest.addHeader(headPair.getKey().toString(),
								headPair.getValue().toString());
					}
				}
			}
			if (null != body) {
				postRequest.setEntity(new StringEntity(body));
			}
			response = client.execute(postRequest);
			return response;
		} catch (Exception exp) {
			throw new BoxException("While executing Http Post " + exp);
		}
	}

	public static HttpResponse executeGet(String path,
			Map<String, String> headers) {
		try {
			HttpGet userContentRequest = new HttpGet(path);
			HttpClient client = new DefaultHttpClient();
			Iterator<Entry<String, String>> it = headers.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				userContentRequest.addHeader(pair.getKey().toString(), pair
						.getValue().toString());
			}
			HttpResponse response = client.execute(userContentRequest);
			return response;
		} catch (Exception exp) {
			throw new BoxException("While executing Http Get " + exp);
		}

	}

	public static HttpResponse executeMultipartPost(String url,
			Map<String, ContentBody> parts, Map<String, String> headers) {
		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost multipPost = new HttpPost(url);
			if (null != headers) {
				Iterator<Entry<String, String>> it = headers.entrySet()
						.iterator();
				while (it.hasNext()) {
					Map.Entry pair = (Map.Entry) it.next();
					multipPost.addHeader(pair.getKey().toString(), pair
							.getValue().toString());
				}
			} else {
				throw new BoxException(
						"Access token header is required for multipart POST request");
			}
			MultipartEntity reqEntity = new MultipartEntity();
			Iterator<Entry<String, ContentBody>> it = parts.entrySet()
					.iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				reqEntity.addPart(pair.getKey().toString(),
						(ContentBody) pair.getValue());
			}
			multipPost.setEntity(reqEntity);
			HttpResponse response = httpclient.execute(multipPost);
			return response;
		} catch (Exception exp) {
			throw new BoxException("While executing Http Multipart Post " + exp);
		}
	}

	public static HttpResponse executeDelete(String url,
			Map<String, String> headers) {
		try {
			HttpDelete delRequest = new HttpDelete(url);
			HttpClient httpClient = new DefaultHttpClient();
			if (null != headers) {
				if (headers.size() > 0) {
					Iterator<Entry<String, String>> itHead = headers.entrySet()
							.iterator();
					while (itHead.hasNext()) {
						Map.Entry headPair = (Map.Entry) itHead.next();
						delRequest.addHeader(headPair.getKey().toString(),
								headPair.getValue().toString());
					}
				}
			}
			HttpResponse response = httpClient.execute(delRequest);
			return response;
		} catch (Exception e) {
			throw new BoxException("While executing Http Delete " + e);
		}
	}
	public static HttpResponse executePut(String url, Map<String, String> headers, String body){
		try{
		HttpPut putRequest = new HttpPut(url);
		HttpClient httpClient = new DefaultHttpClient();
		if (null != headers) {
			if (headers.size() > 0) {
				Iterator<Entry<String, String>> itHead = headers.entrySet()
						.iterator();
				while (itHead.hasNext()) {
					Map.Entry headPair = (Map.Entry) itHead.next();
					putRequest.addHeader(headPair.getKey().toString(),
							headPair.getValue().toString());
				}
			}
		}
		if (null != body) {
			putRequest.setEntity(new StringEntity(body));
		}
		HttpResponse response = httpClient.execute(putRequest);
		return response;
		}catch(Exception e){
			throw new BoxException("While executing Http Put " + e);
		}
	}

}
