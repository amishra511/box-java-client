package com.boxclient.test;

import java.util.List;
import java.util.Scanner;

import com.boxclient.common.BoxMain;
import com.boxclient.exception.BoxException;
import com.boxclient.oauth.Token;
import com.boxclient.types.ContentMetadata;
import com.boxclient.types.collection.ContentMetadataCollection;

public class ContentSearchTest {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		try {
			OAuthTest oauth = new OAuthTest();
			Token tkn = oauth.authorizeUser(in);
			BoxMain box = new BoxMain(tkn.getAccessToken());
			System.out.println("Enter a search string:");
			System.out.print(">>");
			String searchString = (String) in.nextLine();
			ContentMetadataCollection searchReasults = box
					.simpleSearch(searchString);
			if (null != searchReasults) {
				if (searchReasults.getTotalCount().equals(0)) {
					System.out.println("No results found");
				} else {					
					System.out.println("Search Results:");
					System.out.println("Results count:"
							+ searchReasults.getTotalCount());
					List<ContentMetadata> entries = searchReasults.getEntries();
					for (ContentMetadata cmd : entries) {
						System.out.println("--------Result-------");
						System.out.println("Name: " + cmd.getName());
						System.out.println("Size: " + cmd.getSize());
						System.out.println("Created at: "
								+ cmd.getCreatedAt().toString());
						System.out.println("Modified at: "
								+ cmd.getModifiedAt().toString());
					}
				}
			} else {
				System.out.println("Search could not be completed");
			}			
			
		} catch (Exception exp) {
			throw new BoxException("Error while searching content: ", exp);
		}
		finally{
			in.close();
		}
	}

}
