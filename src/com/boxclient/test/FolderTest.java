package com.boxclient.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.boxclient.common.BoxMain;
import com.boxclient.oauth.Token;
import com.boxclient.types.ContentMetadata;
import com.boxclient.types.MiniContentMetadata;
import com.boxclient.types.collection.MiniContentMetadataCollection;

public class FolderTest {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		OAuthTest oauth = new OAuthTest();
		Token tkn = oauth.authorizeUser(in);
		// -- Display root folder contents
		System.out.println("Root folder content:");
		BoxMain box = new BoxMain(tkn.getAccessToken());
		MiniContentMetadataCollection itemCol = box.getFolderItems("0");
		List<MiniContentMetadata> col = itemCol.getEntries();
		if (null != col) {
			for (MiniContentMetadata mcm : col) {
				System.out.println("Id: " + mcm.getId() + " Type: "
						+ mcm.getType() + " Name: " + mcm.getName());
			}
		}

		// -- Display metadata of a folder
		System.out
				.println("Enter the id of the folder you want to know the metadata of");
		System.out.print(">>");
		String folderId = (String) in.nextLine();
		ContentMetadata cmdata = box.getFolderMetadata(folderId);

		// -- Get selective metadata
		// List<String> fields = new ArrayList<String>();
		// fields.add("name");
		// fields.add("created_at");
		// ContentMetadata cmdata = box.getFolderMetadata(folderId, fields);

		if (null != cmdata) {
			System.out.println("Name: " + cmdata.getName());
			System.out.println("Size: " + cmdata.getSize());
			System.out.println("Created at: " + cmdata.getCreatedAt());
			System.out.println("Modified at: " + cmdata.getModifiedAt());
			System.out.println("Description: " + cmdata.getDescription());
			System.out.println("Items in folder: "
					+ cmdata.getItemCollection().getTotalCount());
		}

		// -- Create a new folder (under root folder)
		System.out.println("Enter the name of new folder you want to create");
		System.out.print(">>");
		String newFolderName = (String) in.nextLine();
		ContentMetadata newFolder = box.createFolder(newFolderName, "0");
		if (null != newFolder) {
			System.out.println("----New Folder----");
			System.out.println("Id: " + newFolder.getId());
			System.out.println("Name: " + newFolder.getName());
			System.out.println("Description: " + newFolder.getName());
			System.out.println("Parnet Name: "
					+ newFolder.getParent().getName());
			System.out.println("Created at: " + newFolder.getCreatedAt());
		}

		// -- Delete a folder and its content recursively
		System.out.println("Enter the id of new folder you want to delete");
		System.out.print(">>");
		String delFolderId = (String) in.nextLine();
		box.deleteFolder(delFolderId);

		// -- Update Folder information e.g description
		System.out.println("Enter the id of folder you want to update");
		System.out.print(">>");
		String fldId = (String) in.nextLine();
		System.out.println("Enter the new description of folder");
		System.out.print(">>");
		String newDesc = (String) in.nextLine();
		ContentMetadata updMdata = new ContentMetadata();
		updMdata.setDescription(newDesc);
		ContentMetadata updatedFolder = box.updateFolderMetadata(fldId,
				updMdata);
		System.out.println("-- Updated Folder --");
		System.out.println("Id: " + updatedFolder.getId());
		System.out.println("Name: " + updatedFolder.getName());
		System.out.println("Description: " + updatedFolder.getDescription());
		
		in.close();
	}

}
