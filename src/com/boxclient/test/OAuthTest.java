package com.boxclient.test;

import java.util.Scanner;

import com.boxclient.common.BoxMain;
import com.boxclient.oauth.OAuthClientImpl;
import com.boxclient.oauth.Token;

public class OAuthTest {
	private static final String CLIENT_ID = "9ax2cu0fcd3jko14fiqi9ziu44kxaxip"; 
	private static final String CLIENT_SECRET = "5n4HtXUCx4Mo8tStpSTfNhk2jrpaYxm3"; 
	private static final String REDIRECT_URI = "https://www.google.com/";
	private static final String UNIQUE_USER_IDENTIFIER = "user123";
	
	public Token authorizeUser(Scanner in){	
	try{		
		OAuthClientImpl oauth = new OAuthClientImpl();
		String authUrl = oauth.getAuthUrl(CLIENT_ID, REDIRECT_URI, UNIQUE_USER_IDENTIFIER);
		/*************************************************/
		System.out.println("Go to the below url and complete the auth process :");
		System.out.println(authUrl);
		System.out.println("Then copy the value of 'code' request parameter from redirect uri(google.com) and paste it here");
		System.out.print(">>");
		String code = (String) in.nextLine();		
		/****************************************************/
		Token tkn = oauth.fetchAcessToken(code, CLIENT_ID, CLIENT_SECRET);		
		return tkn;
	}catch(Exception e){
		e.printStackTrace();
	}
	return null;
	}
}
