package com.boxclient.test;

import java.util.List;
import java.util.Scanner;

import com.boxclient.common.BoxEnums;
import com.boxclient.common.BoxMain;
import com.boxclient.exception.BoxException;
import com.boxclient.oauth.Token;
import com.boxclient.types.ContentMetadata;
import com.boxclient.types.MiniContentMetadata;
import com.boxclient.types.collection.MiniContentMetadataCollection;

public class FileTest {

	public static void main(String[] args) {
		try{
		Scanner in = new Scanner(System.in);
		OAuthTest oauth = new OAuthTest();
		Token tkn =  oauth.authorizeUser(in);
		/**********************************************************/
		System.out.println("Root Folder content:");
		BoxMain box = new BoxMain(tkn.getAccessToken());
		//Use folder id = '0' for root folder
		MiniContentMetadataCollection itemCol = box.getFolderItems("0");
		List<MiniContentMetadata> col = itemCol.getEntries();
		if (null != col){
			for(MiniContentMetadata mcm: col){
				System.out.println("Id: "+mcm.getId()+" Type: "+mcm.getType()+ " Name: "+mcm.getName());
			}
		}
		
		//Contents of any folder can be displayed by passing its id to getFolderItems
		
		//File upload
		//ContentMetadata cont = box.uploadFile("sample_mpeg4.mp4", "sample_mpeg4.mp4", "12345678");
		//File download
		//box.downloadFile("1234567", "../DownloadedFile.pdf");
		}
		catch(Exception e){
			throw new BoxException("Exception while testing file operations: ", e);
		}
		
		
	}
}
