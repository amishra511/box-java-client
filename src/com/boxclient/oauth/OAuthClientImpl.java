package com.boxclient.oauth;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;

import com.boxclient.types.BoxError;
import com.boxclient.util.BoxUtils;
import com.boxclient.util.HttpUtils;
import com.boxclient.util.StringUtils;
import com.boxclient.common.DefaultJsonMapper;
import com.boxclient.exception.BoxException;
import com.boxclient.exception.OAuthException;
import com.boxclient.json.JSONObject;

public class OAuthClientImpl implements OAuthClient {
	private static final String TOKEN_URL = "https://api.box.com/oauth2/token";
	// private static final String TOKEN_URL =
	// "https://app.box.com/api/oauth2/token";
	private static final String REVOKE_URL = "https://api.box.com/oauth2/revoke";
	private static final String AUTHORIZE_URL = "https://app.box.com/api/oauth2/authorize?";
	public static final String ENCODING_CHARSET = "UTF-8";

	private DefaultJsonMapper jsonMapper;

	public OAuthClientImpl() {
		this.jsonMapper = new DefaultJsonMapper();
	}

	public String getAuthUrl(String clientId, String redirectUri, String state) {
		try {
			String authUrl = AUTHORIZE_URL + "response_type="
					+ URLEncoder.encode("code", ENCODING_CHARSET)
					+ "&client_id="
					+ URLEncoder.encode(clientId, ENCODING_CHARSET)
					+ "&redirect_uri="
					+ URLEncoder.encode(redirectUri, ENCODING_CHARSET)
					+ "&state=" + URLEncoder.encode(state, ENCODING_CHARSET);
			return authUrl;
		} catch (Exception e) {
			throw new OAuthException(
					"Exception while creating authorization url :",
					e);
		}
	}

	public String getAuthUrl(String clientId, String redirectUri, String state,
			List<String> scope, String folderId) {
		try {
			StringBuilder authUrl = new StringBuilder(getAuthUrl(clientId,
					redirectUri, state, scope));
			authUrl.append("&folder_id="
					+ URLEncoder.encode(folderId, ENCODING_CHARSET));
			return authUrl.toString();
		} catch (Exception e) {
			throw new OAuthException(
					"Exception while creating authorization url :",
					e);
		}
	}

	public String getAuthUrl(String clientId, String redirectUri, String state,
			List<String> scope) {
		try {
			StringBuilder authUrl = new StringBuilder(getAuthUrl(clientId,
					redirectUri, state));
			StringBuilder scopeParams = null;
			scopeParams = new StringBuilder();
			for (String sc : scope) {
				scopeParams.append(sc);
				scopeParams.append(",");
			}
			scopeParams.delete(scopeParams.length() - 1, scopeParams.length());
			authUrl.append("&scope="
					+ URLEncoder.encode(scopeParams.toString(),
							ENCODING_CHARSET));
			return authUrl.toString();
		} catch (UnsupportedEncodingException e) {
			throw new OAuthException(
					"Exception while creating authorization url :",
					e);
		}
	}

	public String getAuthUrl(String clientId, String redirectUri, String state,
			String folderId) {
		try {
			StringBuilder authUrl = new StringBuilder(getAuthUrl(clientId,
					redirectUri, state));
			authUrl.append("&folder_id="
					+ URLEncoder.encode(folderId, ENCODING_CHARSET));
			return authUrl.toString();
		} catch (UnsupportedEncodingException e) {
			throw new OAuthException(
					"Exception while creating authorization url :",
					e);
		}
	}

	public String getAuthPage(String clientId, String redirectUri, String state) {
		try {
			Map<String, String> params = new HashMap<String, String>();
			params.put("response_type", "code");
			params.put("client_id", clientId);
			params.put("redirect_uri", redirectUri);
			params.put("state", state);
			HttpResponse response = HttpUtils.executePost(AUTHORIZE_URL,
					params, null, null);

			HttpEntity entity = null;
			if (null != response) {
				entity = response.getEntity();
			}

			String output = null;
			if (null != entity) {
				try {
					output = EntityUtils.toString(entity);
				} catch (ParseException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return output;
		} catch (Exception exp) {
			throw new OAuthException(
					"Exception while fetching authorization page :",
					exp);
		}
	}

	public String fetchRefreshToken(String code, String clientId,
			String clientSecret) {
		try {
			Token token = fetchAcessToken(code, clientId, clientSecret);
			return token.getRefreshToken();
		} catch (Exception e) {
			throw new OAuthException(
					"Exception while fetching refresh token :",
					e);
		}

	}

	public Token fetchAcessToken(String code, String clientId,
			String clientSecret) {
		try {
			Map<String, String> params = new HashMap<String, String>();
			params.put("grant_type", "authorization_code");
			params.put("code", code);
			params.put("client_id", clientId);
			params.put("client_secret", clientSecret);
			HttpResponse response = HttpUtils.executePost(TOKEN_URL, params,
					null, null);
			HttpEntity entity = response.getEntity();
			String output = EntityUtils.toString(entity);
			if(StringUtils.isBlank(output)){
				throw new OAuthException("Empty response from Box API. Verify the parameter values");
			}
			if (!BoxUtils.returnedError(output)) {
				Token token = jsonMapper.toJavaObject(output, Token.class);
				if (null != token) {
					token.setClientId(clientId);
					token.setClientSecret(clientSecret);
					return token;
				}
			} else {
				BoxError error = jsonMapper
						.toJavaObject(output, BoxError.class);
				throw new OAuthException(error.getCode()
						+ " - "
						+ error.getContextInfo().getErrors().get(0)
								.getMessage());
			}
		} catch (Exception e) {
			throw new OAuthException(
					"Exception while fetching access token: ",
					e);
		}
		return null;
	}

	public Token fetchAccessTokenFromRefreshToken(String refreshToken,
			String clientId, String clientSecret) {
		try {
			Map<String, String> params = new HashMap<String, String>();
			params.put("grant_type", "refresh_token");
			params.put("refresh_token", refreshToken);
			params.put("client_id", clientId);
			params.put("client_secret", clientSecret);
			HttpResponse response = HttpUtils.executePost(TOKEN_URL, params,
					null, null);
			HttpEntity entity = response.getEntity();
			String output = EntityUtils.toString(entity);
			if(StringUtils.isBlank(output)){
				throw new OAuthException("Empty response from Box API. Verify the parameter values");
			}
			if (!BoxUtils.returnedError(output)) {
				Token token = jsonMapper.toJavaObject(output, Token.class);
				if (null != token) {
					token.setClientId(clientId);
					token.setClientSecret(clientSecret);
					return token;
				}
			} else {
				BoxError error = jsonMapper
						.toJavaObject(output, BoxError.class);
				throw new OAuthException(error.getCode()
								+ " - "
								+ error.getContextInfo().getErrors().get(0)
										.getMessage());
			}
		} catch (Exception e) {
			throw new OAuthException(
					"Exception while fetching access token from refresh token: ",
					e);
		}
		return null;
	}

	// Any of refresh or access token as token parameter, both will be invalid
	// as the result
	public String revokeTokens(String token, String clientId,
			String clientSecret) {
		try {
			String status = null;
			Map<String, String> params = new HashMap<String, String>();
			params.put("client_id", clientId);
			params.put("client_secret", clientSecret);
			params.put("token", token);
			HttpResponse response = HttpUtils.executePost(REVOKE_URL, params,
					null, null);
			if (null == response) {
				status = "success";
			} else {
				HttpEntity entity = response.getEntity();
				String output = EntityUtils.toString(entity);
				if (BoxUtils.returnedError(output)) {
					BoxError error = jsonMapper
							.toJavaObject(output, BoxError.class);
					status = error.getMessage();
				} else {
					status = "unknown error";
				}
			}
			return status;
		} catch (Exception exp) {
			throw new OAuthException(
					"Exception while revoking OAuth tokens :",
					exp);
		}
	}

	public DefaultJsonMapper getJsonMapper() {
		return jsonMapper;
	}

	public void setJsonMapper(DefaultJsonMapper jsonMapper) {
		this.jsonMapper = jsonMapper;
	}

}
