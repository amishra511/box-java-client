package com.boxclient.oauth;

import java.sql.Timestamp;
import java.util.List;

import com.boxclient.types.Box;


public class Token {
	private static final String OBJECT_REF_DOC = "https://developers.box.com/docs/#oauth-2-token";
		
    
    @Box("access_token")
	private String accessToken;
    @Box("expires_in")
	private Double accessTokenExpiresIn;    
    @Box("restricted_to")
	private List<Object> restrictedTo;    
    @Box("refresh_token")
	private String refreshToken;
    @Box("token_type")
	private String tokenType;
    
	private Timestamp refreshTokenExpiresAt;
	private String clientId;
	private String clientSecret;
	private String redirectUri;
	private String userName;
	private String boxDeviceId;
	private String boxDeviceName;	

	public Token() {
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public String getRedirectUri() {
		return redirectUri;
	}

	public void setRedirectUri(String redirectUri) {
		this.redirectUri = redirectUri;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getBoxDeviceId() {
		return boxDeviceId;
	}

	public void setBoxDeviceId(String boxDeviceId) {
		this.boxDeviceId = boxDeviceId;
	}

	public String getBoxDeviceName() {
		return boxDeviceName;
	}

	public void setBoxDeviceName(String boxDeviceName) {
		this.boxDeviceName = boxDeviceName;
	}



	public static String getObjectRefDoc() {
		return OBJECT_REF_DOC;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public Timestamp getRefreshTokenExpiresAt() {
		return refreshTokenExpiresAt;
	}

	public void setRefreshTokenExpiresAt(Timestamp refreshTokenExpiresAt) {
		this.refreshTokenExpiresAt = refreshTokenExpiresAt;
	}

	public Double getAccessTokenExpiresIn() {
		return accessTokenExpiresIn;
	}

	public void setAccessTokenExpiresIn(Double accessTokenExpiresIn) {
		this.accessTokenExpiresIn = accessTokenExpiresIn;
	}

	public List<Object> getRestrictedTo() {
		return restrictedTo;
	}

	public void setRestrictedTo(List<Object> restrictedTo) {
		this.restrictedTo = restrictedTo;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

}
