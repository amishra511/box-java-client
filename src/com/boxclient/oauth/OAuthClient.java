package com.boxclient.oauth;

import java.util.List;

public interface OAuthClient {
	
	public String getAuthUrl(String clientId, String redirectUri, String state);
	public String getAuthUrl(String clientId, String redirectUri, String state, List<String> scope, String folderId);
	public String getAuthUrl(String clientId, String redirectUri, String state, List<String> scope);
	public String getAuthUrl(String clientId, String redirectUri, String state, String folderId);
	public String getAuthPage(String clientId, String redirectUri, String state);
	public String fetchRefreshToken(String code, String clientId, String clientSecret);	
	public Token fetchAcessToken(String code, String clientId, String clientSecret);
	public Token fetchAccessTokenFromRefreshToken(String refreshToken,String clientId, String clientSecret);
	public String revokeTokens(String token, String clientId, String clientSecret);
}
