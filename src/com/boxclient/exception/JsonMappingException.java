package com.boxclient.exception;

public class JsonMappingException extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 34380547697477331L;

	public JsonMappingException(String message) {
		super(message);
		
	}
	public JsonMappingException(String message, Throwable cause) {
		super(message, cause);
	}

}
