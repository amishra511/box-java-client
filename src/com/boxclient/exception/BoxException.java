package com.boxclient.exception;

public class BoxException extends BaseException {
	/**
	 * 
	 */
	public BoxException(String message) {
		super(message);
	}

	public BoxException(String message, Throwable cause) {
		super(message, cause);
	}

}
