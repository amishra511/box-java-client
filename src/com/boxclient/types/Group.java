package com.boxclient.types;

import java.sql.Timestamp;

public class Group {
	private static final String OBJECT_REF_DOC ="https://developers.box.com/docs/#groups";
	
	private String type;
	private String id;
	private String name;
	private Timestamp createdAt;
	private Timestamp modifiedAt;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
	public Timestamp getModifiedAt() {
		return modifiedAt;
	}
	public void setModifiedAt(Timestamp modifiedAt) {
		this.modifiedAt = modifiedAt;
	}
	public static String getObjectRefDoc() {
		return OBJECT_REF_DOC;
	}

}
