package com.boxclient.types;

import java.sql.Timestamp;

public class Collaboration {
	private static final String OBJECT_REF_DOC ="https://developers.box.com/docs/#collaborations";	
	
	private String type;
	private String id;
	private String createdBy;
	private Timestamp createdAt;
	private Timestamp modifiedAt;
	private Timestamp expiresAt;
	private String status; //Enum
	private MiniUser accessibleByUser;
	private Group accessibleByGroup;
	private String role; //Enum
	private Timestamp acknowledgeAt;
	private MiniContentMetadata item;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
	public Timestamp getModifiedAt() {
		return modifiedAt;
	}
	public void setModifiedAt(Timestamp modifiedAt) {
		this.modifiedAt = modifiedAt;
	}
	public Timestamp getExpiresAt() {
		return expiresAt;
	}
	public void setExpiresAt(Timestamp expiresAt) {
		this.expiresAt = expiresAt;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public MiniUser getAccessibleByUser() {
		return accessibleByUser;
	}
	public void setAccessibleByUser(MiniUser accessibleByUser) {
		this.accessibleByUser = accessibleByUser;
	}
	public Group getAccessibleByGroup() {
		return accessibleByGroup;
	}
	public void setAccessibleByGroup(Group accessibleByGroup) {
		this.accessibleByGroup = accessibleByGroup;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public Timestamp getAcknowledgeAt() {
		return acknowledgeAt;
	}
	public void setAcknowledgeAt(Timestamp acknowledgeAt) {
		this.acknowledgeAt = acknowledgeAt;
	}
	public MiniContentMetadata getItem() {
		return item;
	}
	public void setItem(MiniContentMetadata item) {
		this.item = item;
	}
	public static String getObjectRefDoc() {
		return OBJECT_REF_DOC;
	}

	
}
