package com.boxclient.types;

import java.sql.Timestamp;

public class TaskAssignment {
	private static final String OBJECT_REF_DOC ="https://developers.box.com/docs/#tasks-get-a-task-assignment";
	
	private String type;
	private String id;
	private MiniContentMetadata item;
	private MiniUser assignedTo;
	private String message;
	private Timestamp completedAt;
	private Timestamp assignedAt;
	private Timestamp remindedAt;
	private String resolution_state; //Enum
	private MiniUser assignedBy;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public MiniContentMetadata getItem() {
		return item;
	}
	public void setItem(MiniContentMetadata item) {
		this.item = item;
	}
	public MiniUser getAssignedTo() {
		return assignedTo;
	}
	public void setAssignedTo(MiniUser assignedTo) {
		this.assignedTo = assignedTo;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Timestamp getCompletedAt() {
		return completedAt;
	}
	public void setCompletedAt(Timestamp completedAt) {
		this.completedAt = completedAt;
	}
	public Timestamp getAssignedAt() {
		return assignedAt;
	}
	public void setAssignedAt(Timestamp assignedAt) {
		this.assignedAt = assignedAt;
	}
	public Timestamp getRemindedAt() {
		return remindedAt;
	}
	public void setRemindedAt(Timestamp remindedAt) {
		this.remindedAt = remindedAt;
	}
	public String getResolution_state() {
		return resolution_state;
	}
	public void setResolution_state(String resolution_state) {
		this.resolution_state = resolution_state;
	}
	public MiniUser getAssignedBy() {
		return assignedBy;
	}
	public void setAssignedBy(MiniUser assignedBy) {
		this.assignedBy = assignedBy;
	}
	public static String getObjectRefDoc() {
		return OBJECT_REF_DOC;
	}

}
