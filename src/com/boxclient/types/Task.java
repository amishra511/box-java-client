package com.boxclient.types;

import java.sql.Timestamp;

import com.boxclient.types.collection.TaskAssignmentCollection;

public class Task {
	private static final String OBJECT_REF_DOC ="https://developers.box.com/docs/#tasks";
	
	private String type;
	private String id;
	private MiniContentMetadata item;
	private Timestamp dueAt;
	private String action;
	private String message;
	private TaskAssignmentCollection taskAssignCollection;
	private Boolean isCompleted;
	private MiniUser createdBy;
	private Timestamp createdAt;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public MiniContentMetadata getItem() {
		return item;
	}
	public void setItem(MiniContentMetadata item) {
		this.item = item;
	}
	public Timestamp getDueAt() {
		return dueAt;
	}
	public void setDueAt(Timestamp dueAt) {
		this.dueAt = dueAt;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public Boolean getIsCompleted() {
		return isCompleted;
	}
	public void setIsCompleted(Boolean isCompleted) {
		this.isCompleted = isCompleted;
	}
	public MiniUser getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(MiniUser createdBy) {
		this.createdBy = createdBy;
	}
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
	public static String getObjectRefDoc() {
		return OBJECT_REF_DOC;
	}
	public TaskAssignmentCollection getTaskAssignCollection() {
		return taskAssignCollection;
	}
	public void setTaskAssignCollection(
			TaskAssignmentCollection taskAssignCollection) {
		this.taskAssignCollection = taskAssignCollection;
	}
	

}
