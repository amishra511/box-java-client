package com.boxclient.types;

import java.sql.Timestamp;

public class MiniContentMetadata {
	@Box
	private String type;
	@Box
	private String id;
	@Box
	private String name;
	@Box("sequence_id")
	private String seqId;
	@Box
	private String etag;
	@Box
	private MiniContentMetadata parent;
	@Box
	private String sha1; //Used only for file type
	@Box("created_at")
	private Timestamp createdAt;
	@Box("modified_at")
	private Timestamp modifiedAt;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSeqId() {
		return seqId;
	}
	public void setSeqId(String seqId) {
		this.seqId = seqId;
	}
	public String getEtag() {
		return etag;
	}
	public void setEtag(String etag) {
		this.etag = etag;
	}
	public String getSha1() {
		return sha1;
	}
	public void setSha1(String sha1) {
		this.sha1 = sha1;
	}
	public MiniContentMetadata getParent() {
		return parent;
	}
	public void setParent(MiniContentMetadata parent) {
		this.parent = parent;
	}
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
	public Timestamp getModifiedAt() {
		return modifiedAt;
	}
	public void setModifiedAt(Timestamp modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

}
