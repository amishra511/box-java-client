package com.boxclient.types.param;

import java.sql.Timestamp;
import java.util.List;

public class AdvanceSearchOptParams {
	private String scope;
	private List<String> fileExtensions;
	//fromCreatedDate,toCreatedDate
	private Timestamp fromCreatedDate;
	private Timestamp toCreatedDate;
	private Timestamp fromUpdatedDate;
	private Timestamp toUpdatedDate;
	private Integer fromSize;
	private Integer toSize;
	private List<String> userIds;//one or many
	private List<String> ancestorFolderIds;
	private List<String>contentTypes;
	private String returnType; //file, folder or web_link
	private String metadataFilters; //jsonType
	private Integer resultsLimit;
	private Integer resultsOffset;
	
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public List<String> getFileExtensions() {
		return fileExtensions;
	}
	public void setFileExtensions(List<String> fileExtensions) {
		this.fileExtensions = fileExtensions;
	}
	public Timestamp getFromCreatedDate() {
		return fromCreatedDate;
	}
	public void setFromCreatedDate(Timestamp fromCreatedDate) {
		this.fromCreatedDate = fromCreatedDate;
	}
	public Timestamp getToCreatedDate() {
		return toCreatedDate;
	}
	public void setToCreatedDate(Timestamp toCreatedDate) {
		this.toCreatedDate = toCreatedDate;
	}
	public Timestamp getFromUpdatedDate() {
		return fromUpdatedDate;
	}
	public void setFromUpdatedDate(Timestamp fromUpdatedDate) {
		this.fromUpdatedDate = fromUpdatedDate;
	}
	public Timestamp getToUpdatedDate() {
		return toUpdatedDate;
	}
	public void setToUpdatedDate(Timestamp toUpdatedDate) {
		this.toUpdatedDate = toUpdatedDate;
	}
	public Integer getFromSize() {
		return fromSize;
	}
	public void setFromSize(Integer fromSize) {
		this.fromSize = fromSize;
	}
	public Integer getToSize() {
		return toSize;
	}
	public void setToSize(Integer toSize) {
		this.toSize = toSize;
	}
	public List<String> getUserIds() {
		return userIds;
	}
	public void setUserIds(List<String> userIds) {
		this.userIds = userIds;
	}
	public List<String> getAncestorFolderIds() {
		return ancestorFolderIds;
	}
	public void setAncestorFolderIds(List<String> ancestorFolderIds) {
		this.ancestorFolderIds = ancestorFolderIds;
	}
	public List<String> getContentTypes() {
		return contentTypes;
	}
	public void setContentTypes(List<String> contentTypes) {
		this.contentTypes = contentTypes;
	}
	public String getReturnType() {
		return returnType;
	}
	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}
	public String getMetadataFilters() {
		return metadataFilters;
	}
	public void setMetadataFilters(String metadataFilters) {
		this.metadataFilters = metadataFilters;
	}
	public Integer getResultsLimit() {
		return resultsLimit;
	}
	public void setResultsLimit(Integer resultsLimit) {
		this.resultsLimit = resultsLimit;
	}
	public Integer getResultsOffset() {
		return resultsOffset;
	}
	public void setResultsOffset(Integer resultsOffset) {
		this.resultsOffset = resultsOffset;
	}
	
	
	
	

}
