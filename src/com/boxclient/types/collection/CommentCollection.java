package com.boxclient.types.collection;

import java.util.List;

import com.boxclient.types.Box;
import com.boxclient.types.Comment;

public class CommentCollection extends BaseCollection {
	@Box
	private List<Comment> entries;
	public CommentCollection() {
		super();
	}
	public List<Comment> getEntries() {
		return entries;
	}
	public void setEntries(List<Comment> entries) {
		this.entries = entries;
	}

}
