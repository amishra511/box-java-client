package com.boxclient.types.collection;

import java.util.List;

import com.boxclient.types.Box;
import com.boxclient.types.Collaboration;

public class CollaborationCollection extends BaseCollection{
	@Box
	private List<Collaboration> entries;

	public CollaborationCollection(){
		super();
	}

	public List<Collaboration> getEntries() {
		return entries;
	}

	public void setEntries(List<Collaboration> entries) {
		this.entries = entries;
	}
}
