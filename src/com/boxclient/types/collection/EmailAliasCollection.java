package com.boxclient.types.collection;

import java.util.List;

import com.boxclient.types.Box;
import com.boxclient.types.EmailAlias;

public class EmailAliasCollection extends BaseCollection {
	@Box
	private List<EmailAlias> entries;
	
	public EmailAliasCollection() {
		super();
	}
	public List<EmailAlias> getEntries() {
		return entries;
	}
	public void setEntries(List<EmailAlias> entries) {
		this.entries = entries;
	}

}
