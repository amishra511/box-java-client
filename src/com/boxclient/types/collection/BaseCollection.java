package com.boxclient.types.collection;

import java.util.List;

import com.boxclient.types.Box;

public class BaseCollection {
	@Box("total_count")
	private String totalCount;
	@Box
	private String offset;
	@Box
	private String limit;
	@Box
	private List<CollectionOrder> order;

	public String getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(String totalCount) {
		this.totalCount = totalCount;
	}

	public String getOffset() {
		return offset;
	}

	public void setOffset(String offset) {
		this.offset = offset;
	}

	public String getLimit() {
		return limit;
	}

	public void setLimit(String limit) {
		this.limit = limit;
	}

	public List<CollectionOrder> getOrder() {
		return order;
	}

	public void setOrder(List<CollectionOrder> order) {
		this.order = order;
	}

	public static class CollectionOrder {
		@Box
		private String by;
		@Box
		private String direction;

		public String getBy() {
			return by;
		}

		public void setBy(String by) {
			this.by = by;
		}

		public String getDirection() {
			return direction;
		}

		public void setDirection(String direction) {
			this.direction = direction;
		}
	}
}
