package com.boxclient.types.collection;

import java.util.List;

import com.boxclient.types.Box;
import com.boxclient.types.FileVersion;

public class FileVersionCollection extends BaseCollection {
	@Box
	private List<FileVersion> entries;
	public FileVersionCollection() {
		super();
	}
	public List<FileVersion> getEntries() {
		return entries;
	}
	public void setEntries(List<FileVersion> entries) {
		this.entries = entries;
	}

}
