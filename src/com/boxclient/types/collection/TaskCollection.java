package com.boxclient.types.collection;

import java.util.List;

import com.boxclient.types.Box;
import com.boxclient.types.Task;

public class TaskCollection extends BaseCollection {
	@Box
	private List<Task> entries;
	public TaskCollection() {
		super();
	}
	public List<Task> getEntries() {
		return entries;
	}
	public void setEntries(List<Task> entries) {
		this.entries = entries;
	}

}
