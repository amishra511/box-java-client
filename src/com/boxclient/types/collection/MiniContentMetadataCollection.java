package com.boxclient.types.collection;

import java.util.List;

import com.boxclient.types.Box;
import com.boxclient.types.MiniContentMetadata;

public class MiniContentMetadataCollection extends BaseCollection{
	@Box
	private List<MiniContentMetadata> entries;

	public MiniContentMetadataCollection(){
		super();
	}

	public List<MiniContentMetadata> getEntries() {
		return entries;
	}

	public void setEntries(List<MiniContentMetadata> entries) {
		this.entries = entries;
	}
}
