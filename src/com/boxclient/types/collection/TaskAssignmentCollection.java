package com.boxclient.types.collection;

import java.util.List;

import com.boxclient.types.Box;
import com.boxclient.types.TaskAssignment;

public class TaskAssignmentCollection extends BaseCollection{
	@Box
	private List<TaskAssignment> entries;
	
	public TaskAssignmentCollection(){
		super();
	}

	public List<TaskAssignment> getEntries() {
		return entries;
	}

	public void setEntries(List<TaskAssignment> entries) {
		this.entries = entries;
	}
}
