package com.boxclient.types.collection;

import java.util.List;

import com.boxclient.types.Box;
import com.boxclient.types.ContentMetadata;

public class ContentMetadataCollection extends BaseCollection {
	@Box
	private List<ContentMetadata> entries;
	public ContentMetadataCollection(){
		super();
	}
	public List<ContentMetadata> getEntries() {
		return entries;
	}
	public void setEntries(List<ContentMetadata> entries) {
		this.entries = entries;
	}
}
