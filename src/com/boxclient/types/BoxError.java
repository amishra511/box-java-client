package com.boxclient.types;

import java.util.List;

public class BoxError {
	@Box
	private String type;
	@Box
	private Integer status;
	@Box
	private String code;
	@Box("context_info")
	private ContextInfoType contextInfo;
	@Box("help_url")
	private String helpUrl;
	@Box
	private String message;	
	@Box("request_id")
	private String requestId;
	
	public BoxError() {
		// TODO Auto-generated constructor stub
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getHelpUrl() {
		return helpUrl;
	}

	public void setHelpUrl(String helpUrl) {
		this.helpUrl = helpUrl;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public ContextInfoType getContextInfo() {
		return contextInfo;
	}

	public void setContextInfo(ContextInfoType contextInfo) {
		this.contextInfo = contextInfo;
	}

	public static class ContextInfoType{
		@Box
		private List<ErrorInfo> errors;
		@Box
		private List<MiniContentMetadata> conflicts;

		public List<ErrorInfo> getErrors() {
			return errors;
		}

		public void setErrors(List<ErrorInfo> errors) {
			this.errors = errors;
		}

		public List<MiniContentMetadata> getConflicts() {
			return conflicts;
		}

		public void setConflicts(List<MiniContentMetadata> conflicts) {
			this.conflicts = conflicts;
		}
	}
	public static class ErrorInfo{
		@Box
		private String reason;
		@Box
		private String name;
		@Box
		private String message;
		public String getReason() {
			return reason;
		}
		public void setReason(String reason) {
			this.reason = reason;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}		
		
	}

}
