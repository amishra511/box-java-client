package com.boxclient.types;

import java.sql.Timestamp;

public class Invite {
	
	private String type;
	private String id;
	private User invitedTo;
	private User actionableBy;
	private User invitedBy;
	private String status;
	private Timestamp createdAt;
	private Timestamp modifiedAt;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public User getInvitedTo() {
		return invitedTo;
	}
	public void setInvitedTo(User invitedTo) {
		this.invitedTo = invitedTo;
	}
	public User getActionableBy() {
		return actionableBy;
	}
	public void setActionableBy(User actionableBy) {
		this.actionableBy = actionableBy;
	}
	public User getInvitedBy() {
		return invitedBy;
	}
	public void setInvitedBy(User invitedBy) {
		this.invitedBy = invitedBy;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
	public Timestamp getModifiedAt() {
		return modifiedAt;
	}
	public void setModifiedAt(Timestamp modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

}
