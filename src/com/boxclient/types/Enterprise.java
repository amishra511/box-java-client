package com.boxclient.types;

public class Enterprise {
	private static final String OBJECT_REF_DOC ="https://developers.box.com/docs/#users-invite-existing-user-to-join-enterprise";
	
	private String type; //enterprise
	private String id;
	private String name;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public static String getObjectRefDoc() {
		return OBJECT_REF_DOC;
	}
}
