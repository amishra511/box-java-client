package com.boxclient.types;

import java.sql.Timestamp;

public class FileVersion {
	private String type;
	private String id;
	private String sha1;
	private String name;
	private Integer size;
	private Timestamp createdAt;
	private Timestamp modifiedAt;
	private MiniUser modifiedBy;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSha1() {
		return sha1;
	}
	public void setSha1(String sha1) {
		this.sha1 = sha1;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
	public Timestamp getModifiedAt() {
		return modifiedAt;
	}
	public void setModifiedAt(Timestamp modifiedAt) {
		this.modifiedAt = modifiedAt;
	}
	public MiniUser getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(MiniUser modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
}
