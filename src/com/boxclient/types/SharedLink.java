package com.boxclient.types;

public class SharedLink {
	
	private static final String OBJECT_REF_DOC ="https://developers.box.com/get-started/";
    @Box
	private String url;
    @Box("download_url")
	private String downloadUrl;
    @Box("vanity_url")
	private String vanityUrl;
    @Box("is_password_enabled")
	private Boolean isPasswordEnabled;
    @Box("unshared_at")
	private String unsharedAt;
    @Box("download_count")
	private Integer downloadCount;
    @Box("preview_count")
	private Integer previewCount;
    @Box
	private String access; //Enum
    @Box
	private Permissions permissions;
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getDownloadUrl() {
		return downloadUrl;
	}
	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}
	public String getVanityUrl() {
		return vanityUrl;
	}
	public void setVanityUrl(String vanityUrl) {
		this.vanityUrl = vanityUrl;
	}
	public Boolean getIsPasswordEnabled() {
		return isPasswordEnabled;
	}
	public void setIsPasswordEnabled(Boolean isPasswordEnabled) {
		this.isPasswordEnabled = isPasswordEnabled;
	}
	public String getUnsharedAt() {
		return unsharedAt;
	}
	public void setUnsharedAt(String unsharedAt) {
		this.unsharedAt = unsharedAt;
	}
	public Integer getDownloadCount() {
		return downloadCount;
	}
	public void setDownloadCount(Integer downloadCount) {
		this.downloadCount = downloadCount;
	}
	public Integer getPreviewCount() {
		return previewCount;
	}
	public void setPreviewCount(Integer previewCount) {
		this.previewCount = previewCount;
	}
	public String getAccess() {
		return access;
	}
	public void setAccess(String access) {
		this.access = access;
	}
	public Permissions getPermissions() {
		return permissions;
	}
	public void setPermissions(Permissions permissions) {
		this.permissions = permissions;
	}
	public static String getObjectRefDoc() {
		return OBJECT_REF_DOC;
	}
	
	

}
