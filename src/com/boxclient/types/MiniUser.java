package com.boxclient.types;

public class MiniUser {
	private static final String OBJECT_TYPE_DOCS = "https://developers.box.com/docs/#users";
	
	@Box
	private String type;
	@Box
	private String id;
	@Box
	private String name;
	@Box
	private String login;
	
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public static String getObjectTypeDocs() {
		return OBJECT_TYPE_DOCS;
	}
}
