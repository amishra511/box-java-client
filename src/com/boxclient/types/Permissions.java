package com.boxclient.types;

public class Permissions {
	@Box("can_download")
	private Boolean canDownload ;
	@Box("can_preview")
	private Boolean canPreview;
	@Box("can_upload")
	private Boolean canUpload;
	@Box("can_comment")
	private Boolean canComment;
	@Box("can_rename")	
	private Boolean canRename;
	@Box("can_delete")
	private Boolean canDelete;
	@Box("can_share")
	private Boolean canShare;
	@Box("can_invite_collaborator")
	private Boolean canInviteCollaborator;	
	@Box("can_set_share_access")
	private Boolean canSetShareAccess;
	
	public Boolean getCanDownload() {
		return canDownload;
	}
	public void setCanDownload(Boolean canDownload) {
		this.canDownload = canDownload;
	}
	public Boolean getCanPreview() {
		return canPreview;
	}
	public void setCanPreview(Boolean canPreview) {
		this.canPreview = canPreview;
	}
	public Boolean getCanUpload() {
		return canUpload;
	}
	public void setCanUpload(Boolean canUpload) {
		this.canUpload = canUpload;
	}
	public Boolean getCanComment() {
		return canComment;
	}
	public void setCanComment(Boolean canComment) {
		this.canComment = canComment;
	}
	public Boolean getCanRename() {
		return canRename;
	}
	public void setCanRename(Boolean canRename) {
		this.canRename = canRename;
	}
	public Boolean getCanDelete() {
		return canDelete;
	}
	public void setCanDelete(Boolean canDelete) {
		this.canDelete = canDelete;
	}
	public Boolean getCanShare() {
		return canShare;
	}
	public void setCanShare(Boolean canShare) {
		this.canShare = canShare;
	}
	public Boolean getCanSetShareAccess() {
		return canSetShareAccess;
	}
	public void setCanSetShareAccess(Boolean canSetShareAccess) {
		this.canSetShareAccess = canSetShareAccess;
	};

}
