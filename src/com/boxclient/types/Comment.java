package com.boxclient.types;

import java.sql.Timestamp;

public class Comment {
	private static final String OBJECT_REF_DOC ="https://developers.box.com/docs/#comments";
	
	private String type;
	private String id;
	private Boolean isReplyComment;
	private String message;
	private String taggedMessage;
	private MiniUser createdBy;
	private Timestamp createdAt;
	private MiniContentMetadata item;
	private Timestamp modifiedAt;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Boolean getIsReplyComment() {
		return isReplyComment;
	}
	public void setIsReplyComment(Boolean isReplyComment) {
		this.isReplyComment = isReplyComment;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTaggedMessage() {
		return taggedMessage;
	}
	public void setTaggedMessage(String taggedMessage) {
		this.taggedMessage = taggedMessage;
	}
	public MiniUser getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(MiniUser createdBy) {
		this.createdBy = createdBy;
	}
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
	public MiniContentMetadata getItem() {
		return item;
	}
	public void setItem(MiniContentMetadata item) {
		this.item = item;
	}
	public Timestamp getModifiedAt() {
		return modifiedAt;
	}
	public void setModifiedAt(Timestamp modifiedAt) {
		this.modifiedAt = modifiedAt;
	}
	public static String getObjectRefDoc() {
		return OBJECT_REF_DOC;
	}
	
	

}
