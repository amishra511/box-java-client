package com.boxclient.types;

import java.sql.Timestamp;

public class Lock {
	@Box
	private String type;
	@Box
	private String id;
	@Box("created_by")
	private MiniUser createdBy;
	@Box("created_at")
	private Timestamp createdAt;
	@Box("expires_at")
	private Timestamp expiresAt;
	@Box("is_download_prevented")
	private Boolean isDownloadPrevented;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public MiniUser getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(MiniUser createdBy) {
		this.createdBy = createdBy;
	}
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
	public Timestamp getExpiresAt() {
		return expiresAt;
	}
	public void setExpiresAt(Timestamp expiresAt) {
		this.expiresAt = expiresAt;
	}
	public Boolean getIsDownloadPrevented() {
		return isDownloadPrevented;
	}
	public void setIsDownloadPrevented(Boolean isDownloadPrevented) {
		this.isDownloadPrevented = isDownloadPrevented;
	}

}
