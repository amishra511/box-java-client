package com.boxclient.types;

import java.sql.Timestamp;

public class GroupMembership {
	private static final String OBJECT_REF_DOC ="https://developers.box.com/docs/#groups-get-a-group-membership-entry";
	private String type;
	private String id;
	private MiniUser user;
	private Group group;
	private String role; //Enum
	private Timestamp createdAt;
	private Timestamp modifiedAt;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public MiniUser getUser() {
		return user;
	}
	public void setUser(MiniUser user) {
		this.user = user;
	}
	public Group getGroup() {
		return group;
	}
	public void setGroup(Group group) {
		this.group = group;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
	public Timestamp getModifiedAt() {
		return modifiedAt;
	}
	public void setModifiedAt(Timestamp modifiedAt) {
		this.modifiedAt = modifiedAt;
	}
	public static String getObjectRefDoc() {
		return OBJECT_REF_DOC;
	}

}
