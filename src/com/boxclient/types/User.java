package com.boxclient.types;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

public class User {
	private static final String OBJECT_TYPE_DOCS = "https://developers.box.com/docs/#users";
	
	private String type;
	private String id;
	private String name;
	private String login;
	private Timestamp createdAt;
	private Timestamp modifiedAt;
	private String role; //enum
	private String language;//enum
	private String timezone;//enum
	private Integer spaceAmt;
	private Integer spaceUsed;
	private Integer maxUploadSize;
	private String status;//Enum
	private String jobTitle;
	private String phone;
	private String address;
	private String avatarUrl;
	
	/* Explicit request attributes */
	private List<Map<String,String>> trackingCodes;
	private Boolean canSeeMngdUsers;
	private Boolean isSyncEnabled;
	private Boolean isExtCollabRestricted;
	private Boolean isExmptFrmDeviceLimits;
	private Boolean isExmptFrmLognVerification;
	private Enterprise enterprise;
	private List<String> myTags;
	private String hostname;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
	public Timestamp getModifiedAt() {
		return modifiedAt;
	}
	public void setModifiedAt(Timestamp modifiedAt) {
		this.modifiedAt = modifiedAt;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getTimezone() {
		return timezone;
	}
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	public Integer getSpaceAmt() {
		return spaceAmt;
	}
	public void setSpaceAmt(Integer spaceAmt) {
		this.spaceAmt = spaceAmt;
	}
	public Integer getSpaceUsed() {
		return spaceUsed;
	}
	public void setSpaceUsed(Integer spaceUsed) {
		this.spaceUsed = spaceUsed;
	}
	public Integer getMaxUploadSize() {
		return maxUploadSize;
	}
	public void setMaxUploadSize(Integer maxUploadSize) {
		this.maxUploadSize = maxUploadSize;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAvatarUrl() {
		return avatarUrl;
	}
	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}
	public List<Map<String, String>> getTrackingCodes() {
		return trackingCodes;
	}
	public void setTrackingCodes(List<Map<String, String>> trackingCodes) {
		this.trackingCodes = trackingCodes;
	}
	public Boolean getCanSeeMngdUsers() {
		return canSeeMngdUsers;
	}
	public void setCanSeeMngdUsers(Boolean canSeeMngdUsers) {
		this.canSeeMngdUsers = canSeeMngdUsers;
	}
	public Boolean getIsSyncEnabled() {
		return isSyncEnabled;
	}
	public void setIsSyncEnabled(Boolean isSyncEnabled) {
		this.isSyncEnabled = isSyncEnabled;
	}
	public Boolean getIsExtCollabRestricted() {
		return isExtCollabRestricted;
	}
	public void setIsExtCollabRestricted(Boolean isExtCollabRestricted) {
		this.isExtCollabRestricted = isExtCollabRestricted;
	}
	public Boolean getIsExmptFrmDeviceLimits() {
		return isExmptFrmDeviceLimits;
	}
	public void setIsExmptFrmDeviceLimits(Boolean isExmptFrmDeviceLimits) {
		this.isExmptFrmDeviceLimits = isExmptFrmDeviceLimits;
	}
	public Boolean getIsExmptFrmLognVerification() {
		return isExmptFrmLognVerification;
	}
	public void setIsExmptFrmLognVerification(Boolean isExmptFrmLognVerification) {
		this.isExmptFrmLognVerification = isExmptFrmLognVerification;
	}
	public Enterprise getEnterprise() {
		return enterprise;
	}
	public void setEnterprise(Enterprise enterprise) {
		this.enterprise = enterprise;
	}
	public List<String> getMyTags() {
		return myTags;
	}
	public void setMyTags(List<String> myTags) {
		this.myTags = myTags;
	}
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	public static String getObjectTypeDocs() {
		return OBJECT_TYPE_DOCS;
	}
	
	
	

}
