package com.boxclient.types;

public class FolderUploadEmail {
	@Box
	private String access;
	@Box
	private String email;
	public String getAccess() {
		return access;
	}
	public void setAccess(String access) {
		this.access = access;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

}
