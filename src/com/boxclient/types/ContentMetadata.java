package com.boxclient.types;

import java.sql.Timestamp;
import java.util.List;

import com.boxclient.types.collection.MiniContentMetadataCollection;

public class ContentMetadata {
	private static final String OBJECT_REF_DOC ="https://developers.box.com/docs/#folders";
	@Box
	private String type;
	@Box
	private String id;
	@Box("sequence_id")
	private String seqId;
	@Box
	private String etag;
	@Box
	private String name;
	@Box
	private String description;
	@Box
	private String size;
	@Box("path_collection")
	private MiniContentMetadataCollection pathCollection;
	@Box("created_at")
	private Timestamp createdAt;
	@Box("modified_at")
	private Timestamp modifiedAt;
	@Box("trashed_at")
	private Timestamp trashedAt;
	@Box("purged_at")
	private Timestamp purgedAt;
	@Box("content_created_at")
	private Timestamp contentCreatedAt;
	@Box("content_modified_at")
	private Timestamp contentModifiedAt;
	@Box("created_by")
	private MiniUser createdBy;
	@Box("modified_by")
	private MiniUser modifiedBy;
	@Box("owned_by")
	private MiniUser ownedBy;
	@Box("shared_link")
	private SharedLink sharedLink;
	@Box
	private MiniContentMetadata parent;
	@Box("item_status")
	private String itemStatus;
	//Explicit request attributes	
	@Box
	private Permissions permissions; 
	@Box
	private List<String> tags;
	
	/*For file type only*/
	@Box
	private String sha1; 
	//Explicit request file attributes
	@Box("version_number")
	private String versionNo;
	@Box("comment_count")
	private Integer cmntCount;
	@Box
	private Lock lock;
	@Box
	private String extension;
	@Box("is_package")
	private String isPackage;
	
	/*For Folder type only*/
	@Box("folder_upload_email")
	private FolderUploadEmail folderUploadEmail;
	@Box("item_collection")
	private MiniContentMetadataCollection itemCollection;
	//Explicit request folder attributes
	@Box("sync_state")
	private String syncState;
	@Box("has_collaborations")
	private Boolean hasCollaborations;
	@Box("can_non_owners_invite")
	private Boolean canNonOwnerInvite;
	@Box("is_externally_owned")
	private Boolean isExternallyOwned;
	@Box("allowed_shared_link_access_levels")
	private String allwdShrdlinkAccessLvls;
	@Box("allowed_invitee_roles")
	private String allwdInviteeRoles;
	
	public String getType() {
		return type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSeqId() {
		return seqId;
	}
	public String getEtag() {
		return etag;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getSize() {
		return size;
	}
	public MiniContentMetadataCollection getPathCollection() {
		return pathCollection;
	}
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public Timestamp getModifiedAt() {
		return modifiedAt;
	}
	public Timestamp getTrashedAt() {
		return trashedAt;
	}
	public Timestamp getPurgedAt() {
		return purgedAt;
	}
	public Timestamp getContentCreatedAt() {
		return contentCreatedAt;
	}
	public Timestamp getContentModifiedAt() {
		return contentModifiedAt;
	}
	public MiniUser getCreatedBy() {
		return createdBy;
	}
	public MiniUser getModifiedBy() {
		return modifiedBy;
	}
	
	public MiniUser getOwnedBy() {
		return ownedBy;
	}
	public void setOwnedBy(MiniUser ownedBy) {
		this.ownedBy = ownedBy;
	}
	public SharedLink getSharedLink() {
		return sharedLink;
	}
	public MiniContentMetadata getParent() {
		return parent;
	}
	public void setParent(MiniContentMetadata parent) {
		this.parent = parent;
	}
	public String getItemStatus() {
		return itemStatus;
	}
	public Permissions getPermissions() {
		return permissions;
	}
	public void setPermissions(Permissions permissions) {
		this.permissions = permissions;
	}
	public List<String> getTags() {
		return tags;
	}
	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	public String getSha1() {
		return sha1;
	}
	public String getVersionNo() {
		return versionNo;
	}
	public Integer getCmntCount() {
		return cmntCount;
	}
	public Lock getLock() {
		return lock;
	}
	public String getExtension() {
		return extension;
	}
	public String getIsPackage() {
		return isPackage;
	}
	public FolderUploadEmail getFolderUploadEmail() {
		return folderUploadEmail;
	}
	public void setFolderUploadEmail(FolderUploadEmail folderUploadEmail) {
		this.folderUploadEmail = folderUploadEmail;
	}
	public MiniContentMetadataCollection getItemCollection() {
		return itemCollection;
	}
	public String getSyncState() {
		return syncState;
	}
	public void setSyncState(String syncState) {
		this.syncState = syncState;
	}
	public Boolean getHasCollaborations() {
		return hasCollaborations;
	}
	public Boolean getCanNonOwnerInvite() {
		return canNonOwnerInvite;
	}
	public Boolean getIsExternallyOwned() {
		return isExternallyOwned;
	}
	public String getAllwdShrdlinkAccessLvls() {
		return allwdShrdlinkAccessLvls;
	}
	public String getAllwdInviteeRoles() {
		return allwdInviteeRoles;
	}	
	public static String getObjectRefDoc() {
		return OBJECT_REF_DOC;
	}
	

}
