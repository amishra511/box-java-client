package com.boxclient.json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class JSONToJavaMapper {

	private static Map<String, Object> toJavaMap(JSONObject jsnObj ) {
		Map<String, Object> outMap =  new HashMap<String, Object>();
		  Iterator iter = jsnObj.keys();
		  while (iter.hasNext()) {
		    String key = (String) iter.next();
		    Object val = jsnObj.get(key);
		    if (val.getClass() == JSONObject.class) {
		      Map<String, Object> jsnMap = toJavaMap((JSONObject) val);
		      outMap.put(key, jsnMap);
		    } else if (val.getClass() == JSONArray.class) {
		      List<Object> jsnLst = new ArrayList<Object>();
		      JSONArray arr = (JSONArray) val;
		      
		      for (int i = 0; i < arr.length(); i++) {
		        Map<String, Object> jsnMap = new HashMap<String, Object>();
		        Object element = arr.get(i);
		        if (element instanceof JSONObject) {
		        	jsnMap = toJavaMap((JSONObject) element);
		        	jsnLst.add(jsnMap);
		        } else {
		        	jsnLst.add(element);
		        }
		      }
		      outMap.put(key, jsnLst);
		    } else {
		    	outMap.put(key, val);
		    }
		  }
		  return outMap;
		}
}
